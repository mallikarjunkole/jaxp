package in.test.jaxp;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class RandomAccess_xpathParser
{
	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException, XPathExpressionException 
	{
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();		
		DocumentBuilder builder1 = factory.newDocumentBuilder();
		Document doc1 = builder1.parse("E://beautoSystem/TestJAXP/src/in/test/xmlFiles/StudentInfo.xml");
		//Using xpath pasrser
		XPathFactory xPathfactory = XPathFactory.newInstance();
        XPath xpath = xPathfactory.newXPath();
        XPathExpression expression = xpath.compile("/student/studentEducationalInfo/studentStandard/text()");
        NodeList xpathNodeList = (NodeList) expression.evaluate(doc1, XPathConstants.NODESET);
        System.out.println("InquiryType is : " +xpathNodeList.item(0));
	}

}
